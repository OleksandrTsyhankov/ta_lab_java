package exceptions05hw;

import java.util.Scanner;

public class Rectangle {

    public static void main(String[] args) {
        System.out.println("Please, enter rectangle width: ");
        int a = checkInt();
        System.out.println("Please, enter rectangle height: ");
        int b = checkInt();
        System.out.println("Now we know that the area of rectangle is " + squareRectangle(a, b));
    }

    public static int squareRectangle(int a, int b) {
        if (a <= 0 | b <= 0) {
            throw new IllegalArgumentException("You should enter a positive number");
        }
        return a * b;
    }

    public static int checkInt() {
        Scanner scan = new Scanner(System.in);
        int c = 0;
        boolean checkError = true;
        while (checkError) {
            if (scan.hasNextInt())
                c = scan.nextInt();
            else {
                System.out.println("This is a symbol, please enter a number");
                scan.next();
                continue;
            }
            checkError = false;
        }
        return c;
    }
}