package arrays.taskc;

import arrays.Output;

import static arrays.ArrayCreator.arrayCreate;


public class SameElementsExtractor {

    public static void main(String[] args) {

        int indexr = 1;
        int[] sortedarray = arrayCreate();
        Output.print(sortedarray);
        int[] resultarray = new int[sortedarray.length];
        resultarray[0] = sortedarray[0];
        for (int i = 0; i < sortedarray.length -1; i++)
            if (sortedarray[i] == sortedarray[i + 1]) {
                i++;
            } else {
                resultarray[indexr] = sortedarray[i + 1];
                indexr ++;
            }
        Output.print(resultarray);
    }
}