package arrays.taska;

import static arrays.ArrayCreator.arrayCreate;
import static arrays.Output.print;

public class ArrayTransformer {
    public static void main(String[] args) {

        int[] first = arrayCreate();
        int[] second = arrayCreate();

        int[] original = OriginalElement.selectOriginalElements(first, second);
        int[] common = CommonElement.selectCommonElements(first, second);

        print(first);
        print(second);
        print(common);
        print(original);


    }
}
