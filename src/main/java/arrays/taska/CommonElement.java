package arrays.taska;

class CommonElement {
    static int[] selectCommonElements(int[] compare, int[] compareto) {
        int resultindex = 0;
        int[] resultarray = new int[compare.length + compareto.length];
        for (int value : compare) {
            for (int i : compareto)
                if (value == i) {
                    resultarray[resultindex] = value;
                    resultindex++;
                }
        }
        int[] cutarray = new int[resultindex];
        System.arraycopy(resultarray, 0, cutarray, 0, resultindex);
        return cutarray;
    }
}
