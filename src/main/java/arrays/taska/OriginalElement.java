package arrays.taska;

class OriginalElement {

    static int[] selectOriginalElements(int[] compare, int[] compareto) {

        int[] sortedarray = new int[compare.length + compareto.length];
        int[] resultarray = new int[100];

        for (int i = 0; i < compare.length; i++) {
            sortedarray[i] = compare[i];
            sortedarray[i + compareto.length] = compareto[i];
        }
        for (int i = sortedarray.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (sortedarray[j] > sortedarray[j + 1]) {
                    int tmp = sortedarray[j];
                    sortedarray[j] = sortedarray[j + 1];
                    sortedarray[j + 1] = tmp;
                }
            }
        }
        int indexr = 1;
        resultarray[0] = sortedarray[0];
        for (int i = 0; i < sortedarray.length - 1; i++)
            if (sortedarray[i] == sortedarray[i + 1]) {
                i++;
            } else {
                resultarray[indexr] = sortedarray[i + 1];
                indexr++;
            }
        int[] cutarray = new int[indexr];
        System.arraycopy(resultarray, 0, cutarray, 0, indexr);
        return cutarray;
    }
}
